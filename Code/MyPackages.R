#####################################################################################################
#####################################################################################################
#####################################################################################################

#Data Analysis on Airbnb
#Prepared by : Group 11 ; DATA 7001
#Author : Gautam Menon 46108472
#Rights reserved

#####################################################################################################

#Installing and loading required packages
install.packages("dplyr")
install.packages("caret")
install.packages("party")
install.packages("factoextra")
install.packages("NbClust")
install.packages("leaflet")
install.packages("ggplot2")
install.packages("tidyverse")
install.packages("ggthemes")
install.packages("GGally")
install.packages("ggExtra")
install.packages("glmnet")
install.packages("corrplot")
install.packages("kableExtra")
install.packages("plotly")

library(ggplot2)
library(dplyr)
library(party)
library(caret)
library(factoextra)
library(NbClust)
library(leaflet)
library(tidyverse)
library(ggthemes)
library(GGally)
library(ggExtra)
library(glmnet)
library(corrplot)
library(kableExtra)
library(plotly)